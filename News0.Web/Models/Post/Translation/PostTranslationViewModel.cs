﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace News0.Web.Models.Post
{
    public class PostTranslationViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public Domain.Entities.Language Language { get; set; }
        public DateTime PublishDate { get; set; }
        public int PostId { get; set; }
    }
}
