﻿using System;
using System.Collections.Generic;
using System.Text;

namespace News0.Domain.Dtos
{
    public class LanguageDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
